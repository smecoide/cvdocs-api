const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const docRoutes = express.Router();
const PORT = 4000;

let Doc = require('./doc.model');

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://127.0.0.1:27017/docsdb', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

docRoutes.route('/').get(function(req, res) {
    Doc.find(function(err, docs) {
        if (err) {
            console.log(err);
        } else {
            res.json(docs);
        }
    });
});

docRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Doc.findById(id, function(err, doc) {
        res.json(doc);
    });
});

docRoutes.route('/update/:id').post(function(req, res) {
    Doc.findById(req.params.id, function(err, doc) {
        if (!doc)
            res.status(404).send("Datos No Encontrados");
        else        
            doc.doc_title = req.body.doc_title;
            doc.doc_keys = req.body.doc_keys;
            doc.doc_description = req.body.doc_description;
            doc.doc_source = req.body.doc_source;
            doc.doc_type = req.body.doc_type;
            doc.doc_frame = req.body.doc_frame;

            doc.save().then(doc => {
                res.json('Documento Editado!');
            })
            .catch(err => {
                res.status(400).send("Error Editar");
            });
    });
});

docRoutes.route('/remove/:id').post(function(req, res) {
    Doc.findById(req.params.id, function(err, doc) {
        if (!doc)
            res.status(404).send("Datos No Encontrados");
        else        
            doc.doc_title = req.body.doc_title;
            doc.doc_keys = req.body.doc_keys;
            doc.doc_description = req.body.doc_description;
            doc.doc_source = req.body.doc_source;
            doc.doc_type = req.body.doc_type;
            doc.doc_frame = req.body.doc_frame;

            doc.remove().then(doc => {
                res.json('Documento Eliminado!');
            })
            .catch(err => {
                res.status(400).send("Error Eliminar");
            });
    });
});

docRoutes.route('/add').post(function(req, res) {
    let doc = new Doc(req.body);
    doc.save()
    .then(doc => {
        res.status(200).json({'doc': 'Documento agregado'});
    })
    .catch(err => {
        res.status(400).send('Error Crear');
    });
});

app.use('/docs', docRoutes);

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});