# `cvDocsApi`

## `1. get started`

### `nodejs`

version: 10

download link: [https://nodejs.org/es/download](https://nodejs.org/es/download)


### `mongodb (windows)`

version: 4.2

download link: [https://www.mongodb.com/download-center/community](https://www.mongodb.com/download-center/community)

run service:
```
PATH_TO_MONGO\MongoDB\bin\mongod.exe
```

create data_base:
```
PATH_TO_MONGO\MongoDB\Server\4.2\bin\mongo.exe
use docsdb
```


### `mongodb (linux)`

version: 4.2

download link: [https://www.mongodb.com/download-center/community](https://www.mongodb.com/download-center/community)

run service:
```
mongod
```

create data_base:
```
mongo
use docsdb
```

### `nodemon`

```
npm install -g nodemon
```


### `extra packages`

```
npm update
```

## `2. run server`

```
nodemon server
```




