const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Doc = new Schema({
    doc_title: String,
    doc_keys: [String],
    doc_description: String,
    doc_source: String,
    doc_type: String,
    doc_frame: {
        from: String,
        to: String,
        local: String,
    }
});

module.exports = mongoose.model('Doc', Doc);